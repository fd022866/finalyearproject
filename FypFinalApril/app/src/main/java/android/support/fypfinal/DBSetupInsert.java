package android.support.fypfinal;

import android.content.Context;
import android.database.sqlite.SQLiteException;

public class DBSetupInsert {

    /* Variables */
    private final Context context;


    public DBSetupInsert(Context ctx) {
        this.context = ctx;
    }


    public void setupInsertToCategories(String values) {
        try {
            DBAdapter db = new DBAdapter(context);
            db.open();
            db.insert("categories",
                    "_id, category_name, category_parent_id, category_icon, category_note",
                    values);
            db.close();
        } catch (SQLiteException e) {
        }
    }

    public void insertAllCategories() {
        setupInsertToCategories("NULL, 'Bread', '0', '', NULL");
        setupInsertToCategories("NULL, 'Bread', '1', '', NULL");
        setupInsertToCategories("NULL, 'Cereals', '1', '', NULL");
        setupInsertToCategories("NULL, 'Frozen bread and rolls', '1', '', NULL");
        setupInsertToCategories("NULL, 'Pancakes', '1', '', NULL");


        setupInsertToCategories("NULL, 'Dessert and baking', '0', '', NULL");
        setupInsertToCategories("NULL, 'Baking', '6', '', NULL");
        setupInsertToCategories("NULL, 'Biscuit', '6', '', NULL");


        setupInsertToCategories("NULL, 'Drinks', '0', '', NULL");
        setupInsertToCategories("NULL, 'Soda', '9', '', NULL");


        setupInsertToCategories("NULL, 'Fruit and vegetables', '0', '', NULL");
        setupInsertToCategories("NULL, 'Frozen fruits and vegetables', '11', '', NULL");
        setupInsertToCategories("NULL, 'Fruit', '11', '', NULL");
        setupInsertToCategories("NULL, 'Vegetables', '11', '', NULL");
        setupInsertToCategories("NULL, 'Canned fruits and vegetables', '11', '', NULL");


        setupInsertToCategories("NULL, 'Health', '0', '', NULL");
        setupInsertToCategories("NULL, 'Meal substitutes', '16', '', NULL");
        setupInsertToCategories("NULL, 'Protein bars', '16', '', NULL");
        setupInsertToCategories("NULL, 'Protein powder', '16', '', NULL");


        setupInsertToCategories("NULL, 'Meat, chicken and fish', '0', '', NULL");
        setupInsertToCategories("NULL, 'Meat', '20', '', NULL");
        setupInsertToCategories("NULL, 'Chicken', '20', '', NULL");
        setupInsertToCategories("NULL, 'Seafood', '20', '', NULL");


        setupInsertToCategories("NULL, 'Dairy and eggs', '0', '', NULL");
        setupInsertToCategories("NULL, 'Eggs', '24', '', NULL");
        setupInsertToCategories("NULL, 'Cream and sour cream', '24', '', NULL");
        setupInsertToCategories("NULL, 'Yogurt', '24', '', NULL");


        setupInsertToCategories("NULL, 'Dinner', '0', '', NULL");
        setupInsertToCategories("NULL, 'Ready dinner dishes', '28', '', NULL");
        setupInsertToCategories("NULL, 'Pizza', '28', '', NULL");
        setupInsertToCategories("NULL, 'Noodle', '28', '', NULL");
        setupInsertToCategories("NULL, 'Pasta', '28', '', NULL");
        setupInsertToCategories("NULL, 'Rice', '28', '', NULL");
        setupInsertToCategories("NULL, 'Wraps', '28', '', NULL");


        setupInsertToCategories("NULL, 'Cheese', '0', '', NULL");
        setupInsertToCategories("NULL, 'Cream cheese', '35', '', NULL");


        setupInsertToCategories("NULL, 'On bread', '0', '', NULL");
        setupInsertToCategories("NULL, 'Cold meats', '37', '', NULL");
        setupInsertToCategories("NULL, 'Spreads', '37', '', NULL");
        setupInsertToCategories("NULL, 'Jam', '37', '', NULL");


        setupInsertToCategories("NULL, 'Snacks', '0', '', NULL");
        setupInsertToCategories("NULL, 'Nuts', '41', '', NULL");
        setupInsertToCategories("NULL, 'Crisps', '41', '', NULL");
    }



    public void setupInsertToFood(String values) {
        try {
            DBAdapter db = new DBAdapter(context);
            db.open();
            db.insert("food",
                    "_id, food_name, food_manufacturer_name, food_serving_size_gram, food_serving_size_gram_measurement, food_serving_size_pcs, food_serving_size_pcs_measurement, food_energy, food_proteins, food_carbohydrates, food_fat, food_energy_calculated, food_proteins_calculated, food_carbohydrates_calculated, food_fat_calculated, food_user_id, food_barcode, food_category_id, food_thumb, food_image_a, food_image_b, food_image_c, food_notes",
                    values);
            db.close();
        } catch (SQLiteException e) {
        }

    }

    // Insert all food into food database
    public void insertAllFood() {

            setupInsertToFood("NULL, 'Bread', 'Hovis', '800', 'gram', '1', 'Pack', '233', '8.7', '44.6', '1.7', '1864', '69.6', '356.8', '13.6', NULL, NULL, '2', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Chocolate Nut Cereal', 'Tesco', '375', 'gram', '1', 'Box', '463', '6.0', '65.7', '18.8', '1607', '22', '249', '68.9', NULL, NULL, '3', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Crunchy Nut Cereal', 'Kellogs', '500', 'gram', '1', 'Box', '398', '6', '82', '4.5', '1990', '30', '410', '22.5', NULL, NULL, '3', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'White Hot Dog Rolls', 'Hatting', '6', 'rolls', '1', 'Pack', '254', '8.4', '45.7', '3.3', '1069', '35.4', '192', '13.8', NULL, NULL, '4', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Large Seeded Burger Buns', 'Tesco', '4', 'buns', '1', 'Pack', '267', '8.9', '45.8', '4.4', '960', '32', '164.8', '16', NULL, NULL, '4', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Brioche Buns', 'Tesco', '4', 'buns', '1', 'Pack', '261', '8.5', '49.3', '2.6', '656', '21.6', '124.4', '1.6', NULL, NULL, '4', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Plain Pancakes', 'Abra Ca Debora', '375', 'gram', '1', 'Pack', '225', '6.3', '24.6', '10.7', '900', '23.3', '90.4', '42.8', NULL, NULL, '5', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Sweet Pancakes', 'Abra Ca Debora', '375', 'gram', '1', 'Pack', '338', '8.5', '42.5', '12.8', '1000', '25.2', '97.3', '45.6', NULL, NULL, '5', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Granulated Sugar', 'Tesco', '1', 'kg', '1', 'Pack', '400', '0', '100', '0', '4000', '0', '1000', '0', NULL, NULL, '7', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Plain Flour', 'Tesco', '1.5', 'kg', '1', 'Pack', '357', '9.9', '73.5', '1.7', '3570', '99', '700', '20', NULL, NULL, '7', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Digestive Biscuits', 'Mcvitie', '250', 'gram', '1', 'Pack', '482', '7.0', '63.6', '21.3', '980', '16', '122', '46', NULL, NULL, '8', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Coke', 'Coke', '1', 'Litre', '1', 'Bottle', '180', '0', '11.5', '0', '1800', '0', '115', '0', NULL, NULL, '10', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, '7up', '7up', '1', 'Litre', '1', 'Bottle', '2', '0', '0', '0', '5', '0', '0', '0', NULL, NULL, '10', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Mixed Fruits', 'Tesco', '250', 'gram', '1', 'Can', '26', '1.9', '4.2', '0.2', '65', '5', '11', '1', NULL, NULL, '12', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Mango', 'Tesco', '250', 'gram', '1', 'Can', '26', '1.9', '4.2', '0.2', '65', '5', '11', '1', NULL, NULL, '12', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Lady Apple', 'Tesco', '250', 'gram', '1', 'Can', '26', '1.9', '4.2', '0.2', '65', '5', '11', '1', NULL, NULL, '12', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Fruit Breakfast Topper', 'Tesco', '225', 'gram', '1', 'Pack', '27', '2.8', '1.9', '0.5', '61', '6', '4', '1', NULL, NULL, '12', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Apple', 'Null', '1', 'apple', '1', 'apple', '95', '1', '25', '0', '95', '1', '25', '0', NULL, NULL, '13', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Mango', 'Null', '1', 'mango', '1', 'mango', '80', '0.9', '15', '0.2', '80', '1', '28', '0', NULL, NULL, '13', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Grapes', 'Null', '1', 'Grapes', '1', 'Grapes', '62', '1', '18', '0.2', '80', '1', '28', '0', NULL, NULL, '13', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Orange', 'Null', '1', 'Orange', '1', 'Orange', '70', '1.1', '18', '0.2', '80', '1', '28', '0', NULL, NULL, '13', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Banana', 'Null', '1', 'Banana', '1', 'Banana', '67', '1', '23', '0.2', '80', '1', '28', '0', NULL, NULL, '13', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Strawberry', 'Null', '200', 'gram', '1', 'Strawberry', '62', '0.7', '13.8', '0.2', '124', '1', '28', '0', NULL, NULL, '13', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Grapes', 'Null', '1', 'Grapes', '1', 'Pack', '62', '0.7', '13.8', '0.2', '124', '1', '28', '0', NULL, NULL, '13', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Broccoli', 'Tesco', '300', 'gram', '1', 'Broccoli', '33', '2.8', '7', '0.4', '99', '8', '21', '1', NULL, NULL, '14', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Potato', 'Tesco', '173', 'gram', '1', 'Potato', '0.12', '4.3', '10', '0.2', '0.16', '0', '4.5', '0.2', NULL, NULL, '14', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Cucumber', 'Tesco', '125', 'gram', '1', 'Cucumber', '14', '0.8', '2.2', '0.1', '18', '1', '3', '0', NULL, NULL, '14', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Carrots', 'Tesco', '50', 'gram', '1', 'Carrot', '12', '0.8', '1.5', '0.1', '6', '0', '1', '0', NULL, NULL, '14', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Onions', 'Tesco', '135', 'gram', '1', 'Onion', '140', '3', '3.1', '11.2', '189', '4', '4', '15', NULL, NULL, '14', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Cauliflower', 'Tesco', '36', 'gram', '1', 'Cauliflower', '30', '1', '4.7', '0.4', '11', '0', '2', '0', NULL, NULL, '14', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Cabbage', 'Tesco', '88', 'gram', '1', 'Cabbage', '13', '1.3', '1.6', '0.2', '11', '1', '1', '0', NULL, NULL, '14', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Pineapple', 'Tesco', '420', 'gram', '1', 'Pineapple', '116', '5', '19', '0.5', '487', '21', '80', '2', NULL, NULL, '15', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Beans', 'Asda', '60', 'gram', '1', 'Beans can', '68', '0.8', '3.3', '5.7', '41', '0', '2', '3', NULL, NULL, '15', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Fruit Cocktail', 'Del Monte', '60', 'gram', '1', 'Can', '68', '0.8', '3.3', '5.7', '41', '0', '2', '3', NULL, NULL, '15', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Peach', 'Tesco', '99', 'gram', '1', 'Peach can', '76', '2.3', '14', '1', '75', '2', '14', '1', NULL, NULL, '15', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Spinach', 'Tesco', '420', 'gram', '1', 'Spinach pack', '83', '3.8', '14', '0.6', '349', '16', '59', '3', NULL, NULL, '15', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Broccoli', 'Asda', '420', 'gram', '1', 'Broccoli pack', '95', '5.3', '15', '0.6', '399', '22', '63', '3', NULL, NULL, '15', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Recovery Blend', 'MyProtein', '1', 'kg', '1', 'Pack', '342', '71', '5.8', '3.4', '3420', '710', '58', '34', NULL, NULL, '17', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Whole fuel blend', 'MyProtein', '1', 'kg', '1', 'Pack', '436', '30', '44', '13', '4360', '300', '440', '130', NULL, NULL, '17', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Protein Meal Replacement', 'MyProtein', '500', 'gram', '1', 'Pack', '392', '34', '34', '13', '1960', '170', '170', '65', NULL, NULL, '17', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Protein Chocolate', 'MyProtein', '70', 'gram', '1', 'Bar', '543', '27', '25', '35', '380', '19', '17', '25', NULL, NULL, '18', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Lean Protein Bar', 'MyProtein', '45', 'gram', '1', 'Bar', '339', '40', '24', '10', '153', '18', '11', '4.6', NULL, NULL, '18', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Protein Cookie', 'MyProtein', '75', 'gram', '1', 'Cookie', '427', '50', '26', '13', '321', '38', '20', '9.9', NULL, NULL, '18', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'FlapJack', 'MyProtein', '80', 'gram', '1', 'Bar', '412', '25', '41', '15', '329', '20', '32', '12', NULL, NULL, '18', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Impact Whey Protein', 'MyProtein', '250', 'gram', '1', 'Pack', '412', '82', '4', '7.5', '1030', '210', '10', '19', NULL, NULL, '19', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Vegan Protein', 'MyProtein', '500', 'gram', '1', 'Pack', '363', '71', '11', '2.5', '1815', '355', '55', '12.5', NULL, NULL, '19', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Protein Granola', 'MyProtein', '750', 'gram', '1', 'Box', '403', '37', '4.5', '9.9', '2900', '265', '32.5', '71.2', NULL, NULL, '19', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Vitamin C Powder', 'MyProtein', '100', 'gram', '1', 'Pack', '2000', '0', '0', '0', '2000', '0', '0', '0', NULL, NULL, '19', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Vitafiber', 'MyProtein', '500', 'gram', '1', 'Pack', '202', '0', '5', '0', '1010', '0', '25', '0', NULL, NULL, '19', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Taurine Powder', 'MyProtein', '500', 'gram', '1', 'Pack', '2', '1', '0', '0', '2', '0', '0', '0', NULL, NULL, '19', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Collagen Protein', 'MyProtein', '1', 'kg', '1', 'Pack', '360', '90', '0', '0', '3600', '900', '0', '0', NULL, NULL, '19', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Lean Steak Mince', 'Tesco', '23', 'gram', '1', 'Pack', '301', '14', '0.5', '27', '69', '3', '0', '6', NULL, NULL, '21', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Lamb Half Leg Joint', 'Tesco', '200', 'gram', '1', 'Leg', '123', '19', '0', '5', '246', '38', '0', '10', NULL, NULL, '21', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, '12 Beef Meatballs', 'Tesco', '200', 'gram', '1', 'Pack', '123', '19', '0', '1.9', '246', '38', '0', '4', NULL, NULL, '21', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Beef Roasting Joint', 'Tesco', '200', 'gram', '1', 'Join', '147', '21', '0', '7', '294', '42', '0', '14', NULL, NULL, '21', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Beef Stir Fry Strips', 'Tesco', '192', 'gram', '1', 'Pack', '110', '24', '0', '1.6', '211', '46', '0', '3', NULL, NULL, '21', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Chicken Breast Portions', 'Tesco', '412', 'gram', '1', 'Pack', '179', '16.8', '0.1', '12.2', '737', '69', '0', '50', NULL, NULL, '22', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Large Whole Chicken', 'Tesco', '200', 'gram', '1', 'Chicken', '93', '21', '0.2', '0.9', '186', '42', '0', '2', NULL, NULL, '20', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Whole Medium Chicken', 'Tesco', '100', 'gram', '1', 'Chicken', '113', '25', '0.2', '1.4', '113', '25', '0', '1', NULL, NULL, '22', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Large Cod Fillets', 'Tesco', '200', 'gram', '1', 'Pack', '79', '16', '1.5', '1', '158', '32', '3', '2', NULL, NULL, '23', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Cripsy Batter Fish Fingers', 'Tesco', '200', 'gram', '1', 'Pack', '79', '16', '1.5', '1', '158', '32', '3', '2', NULL, NULL, '23', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Crispy Lemon Basa Fillets', 'Tesco', '200', 'gram', '1', 'Pack', '79', '16', '1.5', '1', '158', '32', '3', '2', NULL, NULL, '23', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Egg EggWhite', 'Tesco', '30', 'gram', '1', 'Pack', '42', '10.2', '0.4', '0', '13', '3', '0', '0', NULL, NULL, '25', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Egg Yolk', 'Null', '15', 'gram', '1', 'Egg', '308', '15.8', '0.2', '27.1', '46', '2', '0', '4', NULL, NULL, '25', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Boiled Egg', 'Null', '63', 'gram', '1', 'Egg', '143', '12.4', '0.3', '10.2', '90', '8', '0', '6', NULL, NULL, '25', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Egg', 'Tesco', '63', 'gram', '1', 'Egg', '142', '12.4', '0.3', '10.1', '89', '8', '0', '6', NULL, NULL, '25', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Cottage Cheese original', 'Tesco', '250', 'gram', '1', 'Pack', '96', '13', '1.5', '4.3', '240', '33', '4', '11', NULL, NULL, '26', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Cheese', 'Tine', '200', 'Asda', '0.5', 'Pack', '79', '13', '2.1', '2', '158', '26', '4', '4', NULL, NULL, '26', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Natural Cottage Cheese', 'Tesco', '250', 'gram', '1', 'Pack', '92', '11', '7.6', '1.7', '230', '28', '19', '4', NULL, NULL, '26', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Raspberry Yogurt', 'Tesco', '160', 'gram', '1', 'Pack', '58', '10', '4', '0.2', '93', '16', '6', '0', NULL, NULL, '27', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Mixed Fruit Yogurt', 'Tesco', '160', 'gram', '1', 'Pack', '58', '10', '4', '0.2', '93', '16', '6', '0', NULL, NULL, '27', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Vanilla Yogurt', 'Tesco', '170', 'gram', '1', 'Pack', '108', '10', '15', '0.4', '184', '17', '26', '1', NULL, NULL, '27', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Natural Yougurt', 'Tesco', '195', 'gram', '1', 'Pack', '125', '4.4', '20', '2.9', '244', '9', '39', '6', NULL, NULL, '27', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, '0% Fat Greek Style Yogurt', 'Tesco', '195', 'gram', '1', 'Pack', '126', '4.5', '20', '3', '246', '9', '39', '6', NULL, NULL, '27', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Yogurt Vanilla & Granola', 'Tesco', '125', 'gram', '1', 'Pack', '63', '8', '6.8', '0.4', '79', '10', '9', '1', NULL, NULL, '27', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Chicken Curry', 'Homemade', '485', 'gram', '1', 'Serving', '81', '4.1', '14', '3', '393', '20', '68', '15', NULL, NULL, '29', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Korma', 'Tesco', '1', 'serving', '605', 'cal', '0.61', '43.8', '20.7', '38.2', '0.61', '43.8', '20.7', '38.2', NULL, NULL, '29', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Biryani', 'Homemade', '200', 'gram', '1', 'Serving', '0.15', '10', '15.5', '4.7', '0.29', '20', '31', '9.4', NULL, NULL, '29', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Perpperoni Pizza', 'Tesco', '330', 'gram', '1', 'pizza', '254', '10', '23', '13', '838', '33', '76', '43', NULL, NULL, '30', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Noodles', 'Tesco', '85', 'gram', '1', 'Pack', '349', '11.2', '66.6', '3.5', '297', '10', '57', '3', NULL, NULL, '31', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Fusilli Pasta Twists', 'Tesco', '60', 'gram', '1', 'Pack', '350', '14', '65', '2', '210', '8', '39', '1', NULL, NULL, '32', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Wheat Fusilli Pasta', 'Tesco', '60', 'gram', '1', 'Pack', '350', '14', '65', '2', '210', '8', '39', '1', NULL, NULL, '32', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Easy Cook Long Rice', 'Tesco', '60', 'gram', '1', 'Pack', '360', '7', '80', '0.9', '216', '4', '48', '1', NULL, NULL, '33', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Basmati Rice', 'Tesco', '70', 'gram', '1', 'Pack', '350', '12', '34', '2', '245', '8', '24', '1', NULL, NULL, '33', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Basmati Rice', 'Tilda', '60', 'gram', '1', 'Pack', '316', '7.1', '78', '0.6', '190', '4', '47', '0', NULL, NULL, '33', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Rajma Rice', 'Laila', '60', 'gram', '1', 'Pack', '344', '8', '73', '2.2', '206', '5', '44', '1', NULL, NULL, '33', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Chicken Caesar', 'Tesco', '15', 'gram', '1', 'Pack', '34', '1.2', '6.5', '0.2', '5', '0', '1', '0', NULL, NULL, '34', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Sweet Chilli Chicken', 'Tesco', '40', 'gram', '1', 'Pack', '332', '9.3', '54.2', '8', '133', '4', '22', '3', NULL, NULL, '34', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Bbq Chicken', 'Tesco', '41', 'gram', '1', 'Pack', '286', '8.3', '46.6', '6', '117', '3', '19', '2', NULL, NULL, '34', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Crispy Fajita', 'Tesco', '62', 'gram', '1', 'Pack', '234', '9.8', '47', '5.8', '145', '6', '29', '4', NULL, NULL, '34', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Teriyaki Chicken ', 'Tesco', '62', 'gram', '1', 'Pack', '300', '8.5', '53', '5', '186', '5', '33', '3', NULL, NULL, '34', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Chicken Katsu', 'Tesco', '40', 'gram', '1', 'Pack', '280', '8', '47', '5.5', '112', '3', '19', '2', NULL, NULL, '34', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Extra Mature Cheddar Cheese', 'Tesco', '21', 'gram', '1', 'Pack', '152', '7.4', '5.1', '11', '32', '2', '1', '2', NULL, NULL, '36', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Shredded Ham Hock', 'Tesco', '13', 'gram', '1', 'Pack', '173', '17', '1.4', '11', '22', '2', '0', '1', NULL, NULL, '38', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Smoked Ham', 'Tesco', '50', 'gram', '1', 'Pack', '117', '24', '1.3', '1.8', '59', '12', '1', '1', NULL, NULL, '38', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Beef Salami', 'Tesco', '50', 'gram', '1', 'Pack', '103', '21.8', '1', '1.2', '52', '11', '1', '1', NULL, NULL, '38', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Chicken salami', 'Tesco', '9', 'gram', '1', 'Pack', '104', '18', '1.2', '3', '9', '2', '0', '0', NULL, NULL, '38', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Honey Roast Ham', 'Tesco', '50', 'gram', '1', 'Pack', '104', '21', '2', '1.2', '52', '11', '1', '1', NULL, NULL, '38', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Deli Cooked Ham', 'Tesco', '50', 'gram', '1', 'Pack', '197', '20.3', '2', '0.3', '99', '10', '1', '0', NULL, NULL, '38', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Biscuit Spread Smooth', 'Tesco', '30', 'gram', '1', 'Pack', '314', '7', '58', '6', '94', '2', '17', '2', NULL, NULL, '39', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Nutella', 'Ferrero', '30', 'gram', '1', 'Pack', '314', '7', '58', '6', '94', '2', '17', '2', NULL, NULL, '39', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Smooth Peanut Butter', 'Tesco', '30', 'gram', '1', 'Pack', '314', '7', '58', '6', '94', '2', '17', '2', NULL, NULL, '39', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Strawberry Conserve', 'Bonne Maman', '30', 'gram', '1', 'Pack', '193', '0.4', '46', '0.2', '58', '0', '14', '0', NULL, NULL, '40', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Black Cherry', 'Bonne Maman', '30', 'gram', '1', 'Pack', '159', '0.3', '39', '0', '48', '0', '12', '0', NULL, NULL, '40', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Mixed Nuts', 'Tesco', '600', 'gram', '1', 'Pack', '512', '16.1', '37.1', '32.3', '3 072', '97', '223', '194', NULL, NULL, '42', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Max Strong Jalapeno And Cheese Crisps', 'Tesco', '100', 'gram', '1', 'Pack', '389', '7', '35', '19.1', '389', '7', '35', '19', NULL, NULL, '43', 'Null', 'Null', 'Null', 'Null', NULL");
            setupInsertToFood("NULL, 'Variety Multipack Crisps', 'Tesco', '200', 'gram', '1', 'Pack', '504', '7', '58', '26', '1 008', '14', '116', '52', NULL, NULL, '43', 'Null', 'Null', 'Null', 'Null', NULL");
        }


    }

